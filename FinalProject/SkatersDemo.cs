﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FinalProject
{
    class SkatersDemo
    {
        static void Main(string[] args)
        {
            string line;
            int count = 0;

            // declare and initialize skatersArray
            Skaters[] skatersArray = new Skaters[10];
            try
            {
                // pass the file path and file name to the StreamReader constructor
                StreamReader sr = new StreamReader("../../../Pairs.txt");

                // read the text until reach end of file
                while ((line = sr.ReadLine()) != null)
                {
                    // read the line and assign it to the field skater1
                    string skater1 = line;

                    // read the next line and assign to the field skater2
                    string skater2 = sr.ReadLine();

                    // read the next line and assign to the field county
                    string country = sr.ReadLine();

                    // read the next line and assign to the array scores1
                    string[] scores1 = sr.ReadLine().Split(" ");

                    // convert the string array to double array
                    double[] scoresArr1 = Array.ConvertAll(scores1, double.Parse);

                    // read the next line and assign to the array scores2
                    string[] scores2 = sr.ReadLine().Split(" ");

                    // convert the string array to double array 
                    double[] scoresArr2 = Array.ConvertAll(scores2, double.Parse);

                    // store values to the skatersArray
                    skatersArray[count++] = new Skaters(skater1, skater2, country, scoresArr1, scoresArr2);
                }

                // sort the elements of skatersArray using SortSkatersArray method
                Skaters[] sortedSkaters = SortSkatersArray(skatersArray);

                // print the array using PrintSkatersArray method
                PrintSkatersArray(sortedSkaters);
                
                // close the file
                sr.Close();
                Console.ReadLine();

            }
            catch (Exception e)
            {
                Console.WriteLine("Exception" + e.Message);

            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }

        }

        /// <summary>
        /// sort the elements of Skaters[] array using selection sort
        /// </summary>
        /// <param name="skaters"></param>
        /// <returns>sorted Skaters[] array</returns>
        static Skaters[] SortSkatersArray(Skaters[] skaters)
        {
            for (int i = 0; i < skaters.Length - 1; i++)
            {
                double maxScore = skaters[i].CalcFinalScore();
                int maxIndex = i;
                for (int j = i + 1; j < skaters.Length; j++)
                {
                    if (maxScore < skaters[j].CalcFinalScore())
                    {
                        maxIndex = j;
                        maxScore = skaters[j].CalcFinalScore();
                    }
                    
                }

                Skaters temp = skaters[i];
                skaters[i] = skaters[maxIndex];
                skaters[maxIndex] = temp;
            }
            return skaters;
        }

        /// <summary>
        /// print the Skaters[] array and give the special prominence to the medal winners
        /// </summary>
        /// <param name="skaters"></param>
        static void PrintSkatersArray(Skaters[] skaters)
        {
            // declare three lists to store the winners of gold, silver and bronze respectively
            List<Skaters> goldList = new List<Skaters>();
            List<Skaters> silverList = new List<Skaters>();
            List<Skaters> bronzeList = new List<Skaters>();

            // the first element in the array is the gold winner
            goldList.Add(skaters[0]);

            // declare and initialize i to be 1
            int i = 1;

            // this loop begins from 1
            for (; i < skaters.Length; i++)
            {
                /*
                 * if the skaters[i].CalcFinalScore() equals goldList[0].CalcFinalScore()
                 * then add skaters[i] to goldList
                 * otherwise break the loop
                 */
                if (CompareTwoDoubles(goldList[0].CalcFinalScore(), skaters[i].CalcFinalScore()))
                {
                    goldList.Add(skaters[i]);
                }
                else
                {
                    break;
                }
            }

            /*
             * if i is more than 2 when the above loop breaks, then print the result and return
             * otherwise add the skaters[i] to silverList and increment i
             */
            if (i > 2)
            {
                PrintMedalList(goldList, "Gold");
                PrintSkatersWithoutMedal(skaters, i);
                return;
            }
            silverList.Add(skaters[i]);
            i++;

            /*
             * if the skaters[i].CalcFinalScore() equals silverList[0].CalcFinalScore()
             * then add skaters[i] to silverList
             * otherwise break the loop
             */
            for (; i < skaters.Length; i++)
            {
                if (CompareTwoDoubles(silverList[0].CalcFinalScore(), skaters[i].CalcFinalScore()))
                {
                    silverList.Add(skaters[i]);
                }
                else
                {
                    break;
                }
            }

            /*
             * if i is more than 2 when the silverList loop breaks, then print the result and return
             * otherwise add the skaters[i] to bronzeList and increment i
             */
            if (i > 2)
            {
                PrintMedalList(goldList,"Gold");
                PrintMedalList(silverList, "Silver");
                PrintSkatersWithoutMedal(skaters,i);
                return;
            }
            bronzeList.Add(skaters[i]);
            i++;

            /*
            * if the skaters[i].CalcFinalScore() equals bronzeList[0].CalcFinalScore()
            * then add skaters[i] to bronzeList
            * otherwise break the loop
            */
            for (; i < skaters.Length; i++)
            {
                if (CompareTwoDoubles(bronzeList[0].CalcFinalScore(), skaters[i].CalcFinalScore()))
                {
                    bronzeList.Add(skaters[i]);
                }
                else
                {
                    break;
                }
            }

            // print the result
            PrintMedalList(goldList,"Gold");
            PrintMedalList(silverList, "Silver");
            PrintMedalList(bronzeList, "Bronze");
            PrintSkatersWithoutMedal(skaters,i);

        }

        /// <summary>
        /// print the elements of Skaters[] array without any medal
        /// </summary>
        /// <param name="skaters"></param>
        /// <param name="i"></param>
        private static void PrintSkatersWithoutMedal(Skaters[] skaters, int i)
        {
            for (; i < skaters.Length; i++)
            {
                Console.WriteLine(skaters[i] + "\n");
            }
        }

        /// <summary>
        /// print elements of Skaters[] array with medals
        /// </summary>
        /// <param name="medalList"></param>
        /// <param name="medalString"></param>
        private static void PrintMedalList(List<Skaters> medalList, string medalString)
        {
            foreach (var skater in medalList)
            {
                Console.WriteLine(skater + medalString + "\n");
                
            }
        }

        /// <summary>
        /// to compare two doubles
        /// if the difference between the two doubles is equal to or less than 0.000001,
        /// means they are equal
        /// </summary>
        /// <param name="num1"></param>
        /// <param name="num2"></param>
        /// <returns>bool true / false</returns>
        static bool CompareTwoDoubles(double num1, double num2)
        {
            const double DOUBLE_DIFF = 1E-06;
            if (Math.Abs(num1 - num2) <= DOUBLE_DIFF)
            {
                return true;
            }

            return false;
        }
    }
}
