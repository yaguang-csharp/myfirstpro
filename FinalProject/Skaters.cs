﻿namespace FinalProject
{
    class Skaters
    {
        // properties of Skaters
        public string Skater1 { get; set; }
        public string Skater2 { get; set; }
        public string Country { get; set; }
        public double[] ScoreArr1 { get; set; }
        public double[] ScoreArr2 { get; set; }

        // constructor with parameters
        public Skaters(string skater1, string skater2, string country, double[] scoreArr1, double[] scoreArr2)
        {
            Skater1 = skater1;
            Skater2 = skater2;
            Country = country;
            ScoreArr1 = scoreArr1;
            ScoreArr2 = scoreArr2;
        }

        // calculate the final score
        public double CalcFinalScore()
        {
            double total1 = 0;
            double total2 = 0;
            for (int i = 0; i < ScoreArr1.Length; i++)
            {
                total1 += ScoreArr1[i];
                total2 += ScoreArr2[i];
            }
            return total1 / ScoreArr1.Length + total2 / ScoreArr2.Length;
        }

        // override the ToString() method
        public override string ToString()
        {
            double score = this.CalcFinalScore();
            return $"Skater 1: {Skater1,-10}  Skater 2: {Skater2,-10}  Country: {Country, -12}  Score: {score,-10:N4}";
        }
    }
}